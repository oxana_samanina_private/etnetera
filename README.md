# README #

This a demo test

### What is this repository for? ###

* Cypress test for google form
* I divided the form into several Page Objects (one for each component of the form) just for demonstration purposes. 
* I can imagine scenarios with using different set of these components inside of different forms or checking texts or validation (star sign, validation message, some negative scenarios on the components). Thats why I decided to divide the form into small POs.  
* In the real world I would consider having only one PO Form for this single test
* Google form components (like date and time) is set by local setting of the browser. So in different browsers, on different environment (see results from docker image in pipeline artifact's reports) it displays different components. And it is not so easy to fill the value into it as the selector of the component seems to be different too. 

### How do I get set up? ###

* npm i
* npm test - rubs open mode
* npm cy:run:chrome and cy:run:mobile run test in a headless mode with different configuration
* pipeline is set for 2 parallel steps (chrome + mobile viewport)
* you can also download reports and logs from the artifacts  

