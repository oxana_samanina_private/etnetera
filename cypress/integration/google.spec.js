const { Form } = require("../page_objects/form");
const { Helper } = require("../support/helper");

describe("Etnetera Google Test", () => {
  const testData = [
    {
      firstQ: 1,
      secondQ: "Ahoj, Etnetero - test 1",
      thirdQ: [1, 2],
      date: Helper.formatDateToString(new Date(Date.now())),
      hours: new Date(Date.now()).getHours(),
      minutes: new Date(Date.now()).getMinutes(),
      lastQ: 2
    },
    {
      firstQ: 0,
      secondQ: "Ahoj, Etnetero - test 2",
      thirdQ: [2],
      date: Helper.formatDateToString(new Date(Date.now())),
      hours: new Date(Date.now()).getHours(),
      minutes: new Date(Date.now()).getMinutes(),
      lastQ: 1
    },
  ];

  testData.forEach((test) => {
      //GIVEN I am a user on Google form page
      //WHEN I fill all the mandatory fields of the form (with different data set)
      //AND I submit the form
      //THEN form is saved
      //AND I am redirected to the confirmation page  
    it("I fill mandatory fields and submit the form", () => {
      cy.log(`**I run for the next dataset:** Q1:${test.firstQ}, Q2:${test.secondQ}, Q3:${test.thirdQ}, Q4:${test.date} ${test.hours}:${test.minutes}, Q5:${test.lastQ}`);
      cy.visit("/");
      Form
        .selectRadioButton(test.firstQ)
        .fillTextField(test.secondQ)
        .tickCheckboxes(test.thirdQ)
        .setDateAndTime(test.date, test.hours, test.minutes)
        .selectOption(test.lastQ)
        .submit();
      cy.url().should("contains", "/formResponse");
    });
  });
});
