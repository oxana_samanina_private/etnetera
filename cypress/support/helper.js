export const Helper = {
  formatDateToString(date) {
    return date.getFullYear().toString() + "-" + ("0" + (date.getMonth() + 1).toString()).substr(-2) + "-" + ("0" + date.getDate().toString()).substr(-2);
  },
};
