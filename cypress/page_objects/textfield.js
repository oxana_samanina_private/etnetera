const TEXT_FIELD = ".freebirdFormviewerComponentsQuestionTextRoot input";

export const TextField = {
    
  fillInput(text = "test") {
    cy.get(TEXT_FIELD).clear().type(text);
    return this;
  },
};
