const RADIO_GROUP_ROLE = "radiogroup";
const RADIO_ROLE = "radio";

export const Radiogroup = {
    
  selectNthItem(n = 0) {
    cy.findByRole(RADIO_GROUP_ROLE).within(() => {
      cy.findAllByRole(RADIO_ROLE).eq(n).click();
    });
  },
};
