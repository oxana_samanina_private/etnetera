const CHECKBOX_LIST = ".freebirdFormviewerViewItemList";
const CHECKBOX_ROLE = "checkbox";

export const CheckboxList = {
    
  tickCheckboxes(n = [1,2]) {
    cy.get(CHECKBOX_LIST).within(() => {
        n.forEach(i=> {
         cy.findAllByRole(CHECKBOX_ROLE).eq(i).click();
        })
    });
  },
};
