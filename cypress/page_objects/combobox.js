const DROPDOWN = ".exportDropDown";
const POPUP = ".exportSelectPopup";
const OPTIONS = ".exportOption"

export const Combobox = {
    
  selectNthOption(n) {
    cy.get(DROPDOWN).click();
    cy.get(POPUP).within(() => {
      cy.get(OPTIONS).eq(n).click();
    });
  },
};
