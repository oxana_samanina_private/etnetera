const DATE_FIELD = "input[type='date']";
const TIME_FIELDS = ".freebirdFormviewerComponentsQuestionDateTimeInputs input[type='text']"
export const DateAndTime = {
    
  fillDate(date) {
    cy.get(DATE_FIELD).clear().type(date);
    return this;
  },

  fillTime(hours, minutes) {
    cy.get(TIME_FIELDS).eq(0).clear().type(hours);
    cy.get(TIME_FIELDS).eq(1).clear().type(minutes);
    return this;
  },
};
