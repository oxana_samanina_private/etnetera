import { CheckboxList } from "./checkboxlist";
import { Combobox } from "./combobox";
import { DateAndTime, DateTime } from "./datetime";
import { Radiogroup } from "./radiogroup";
import { TextField } from "./textfield";

const BUTTON = "[role='button'].freebirdFormviewerViewNavigationSubmitButton";

export const Form = {
    
  selectRadioButton(n) {
    Radiogroup.selectNthItem(n);
    return this;
  },

  fillTextField(text) {
    TextField.fillInput(text);
    return this;
  },

  tickCheckboxes(n) {
    CheckboxList.tickCheckboxes(n);
    return this;
  },

  setDateAndTime(date, hours, minutes){
    DateAndTime.fillDate(date);
    DateAndTime.fillTime(hours, minutes);
    return this;
  },

  selectOption(n){
      Combobox.selectNthOption(n);
      return this;
  },

  submit(){
      cy.get(BUTTON).click();
      return this;
  }
};
